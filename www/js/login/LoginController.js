/**
 * Created by karthik on 5/9/15.
 */
loginModule.controller('LoginController', function($scope, loginFactory, $cordovaOauth, Common, $state, jwtHelper, $ionicLoading,$ionicSideMenuDelegate, $ionicHistory, $cordovaToast, $rootScope) {
    console.log('In LoginController');

    $ionicSideMenuDelegate.canDragContent(false);
    $scope.formData = {};
    $scope.signUpTitle = Common.messages.SIGNUPTITLE;
    $scope.signUpSubtitle = Common.messages.SIGNUPSUBTITLE;



    var setLocalItems = function(data) {
        localStorage.setItem('id_token', data['api-token']);
        localStorage.setItem('username', data['user']['name']);
        localStorage.setItem('email', data['user']['email']);
        localStorage.setItem('profilepic', data['user']['uimg']);
        localStorage.setItem('user_id', data['user']['uid']);
        localStorage.setItem('guest_mode', 'false');
    };

    $scope.login = function() {
        $ionicHistory.nextViewOptions({
            disableBack: true
        });
        Common.showLoader();
        if($scope.formData.email==null || $scope.formData.email=='' || $scope.formData.password==null || $scope.formData.password=='' || !isEmail($scope.formData.email))  {
            Common.hideLoader();
            Common.showToast(Common.messages.FILLDETAILS);
        }
        else {
            loginFactory.login($scope.formData.email, $scope.formData.password).success(function (data) {
                console.log('Spanking new token. Login');
                if (data['api-token'] != null) {
                    setLocalItems(data);
                    console.log('Login Key set to: ' + data['api-token']);
                    $state.go('app.search');
                    Common.hideLoader();
                }
                else {
                    console.log(data);
                    Common.hideLoader();
                    Common.showToast(data);
                }
            }).error(function (data) {
                console.log('Error');
                Common.hideLoader();
            })
        }
    };

    $scope.loginFacebook = function() {
        $cordovaOauth.facebook("366824603511939", ["public_profile","email", "read_stream", "user_website", "user_location", "user_relationships"]).then(function (result) {
            Common.showLoader();
            loginFactory.loginFacebook({fb_token:result.access_token}).success(function(data) {
                Common.hideLoader();
                if (data['api-token'] != null) {
                    setLocalItems(data);
                    $state.go('app.search');
                }
            })
        }, function (error) {
            console.log(error);
        });
    };

    $scope.loginGplus = function() {
        $cordovaOauth.google("739707660431-qip4kfpjdtd2d93njhkeo4pq4cbcn6bv.apps.googleusercontent.com",  ["PROFILE", "EMAIL"]).then(function (result) {
            Common.showLoader();
            console.log(result);
            loginFactory.loginGplus({gplus_token:result.access_token}).success(function(data) {
                Common.hideLoader();
                if (data['api-token'] != null) {
                    setLocalItems(data);
                    $state.go('app.search');
                }
            })
        });
    };

    $scope.forgotPassword = function(){
        console.log($scope.formData.email);
        Common.showLoader();
        if($scope.formData.email==null || $scope.formData.email=='' || !isEmail($scope.formData.email) )  {
            Common.hideLoader();
            Common.showToast(Common.messages.NOEMAIL);
        }
        else{
            loginFactory.forgotPass({email:$scope.formData.email}).success(function (data) {
                console.log(data);
                Common.hideLoader();
                Common.showToast(data);
            }).error(function (data) {
                console.log('Error');
                Common.hideLoader();
            })
        }
    };

    var isEmail = function(n) {
        var numStr = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
        return numStr.test( n.toString() );
    };
});
