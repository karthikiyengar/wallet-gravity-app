/**
 * Created by karthik on 5/9/15.
 */
loginModule.factory('loginFactory', function($http, Common) {
    return {
        login: function(paramObj1, paramObj2) {
            return $http({
                method: 'POST',
                skipAuthorization: true,
                url: Common.BASE_URL + 'auth/login', 
                data: {
                    'username': paramObj1,
                    'password': paramObj2,
                }
            })
        },
        loginFacebook: function(paramObj) {
            return $http({
                method:'POST',
                skipAuthorization:true,
                url: Common.BASE_URL + 'auth/login/facebook',
                data: paramObj
            })
        },
        loginGplus: function(paramObj) {
            return $http({
                method:'POST',
                skipAuthorization:true,
                url: Common.BASE_URL + 'auth/login/gplus',
                data: paramObj
            })
        },
        userRegister: function(paramObj) {
            return $http({
                method: 'POST',
                skipAuthorization: true,
                url: Common.BASE_URL + 'auth/register',
                data: paramObj
            })
        },
        logout: function(paramObj) {
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'auth/logout'
            })
        },
        forgotPass: function(paramsObj) {
            return $http({
                method: 'POST',
                skipAuthorization: true,
                url: Common.BASE_URL + 'auth/login/forgot-password',
                data: paramsObj
            })
        }
    }
});