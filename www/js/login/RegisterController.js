/**
 * Created by prasad on 5/21/2015.
 */
loginModule.controller('RegisterController', function($scope, loginFactory, $state, $ionicLoading, Common, $cordovaToast) {
    $scope.formData = {};
    window.$scope = $scope;
    $scope.register = function () {         
        Common.showLoader();

        if($scope.formData.signupForm.$error.required != undefined && $scope.formData.signupForm.$error.required.length > 0)  {
            Common.hideLoader();
            Common.showToast(Common.messages.ALLREQUIRED);
        }
        else if($scope.formData.signupForm.username.$invalid) {
            Common.hideLoader();
            Common.showToast(Common.messages.USERNAMELENGTH);
        }
        else if($scope.formData.signupForm.email.$invalid) {
            Common.hideLoader();
            Common.showToast(Common.messages.NOEMAIL);
        }
        else if($scope.formData.signupForm.password.$invalid) {
            Common.hideLoader();
            Common.showToast(Common.messages.PASSWORDLENGTH);
        }
        else {
            var registerObj = {
                "username": $scope.formData.username,
                "email": $scope.formData.email,
                "password": $scope.formData.password
            };
            console.log(registerObj);
            Common.showLoader();
            loginFactory.userRegister(registerObj).success(function (data) {
                Common.hideLoader();
                console.log('Response from server:' + data);
                Common.showToast(data);
                $state.go('auth.login');
            })
            .error(function (data) {
                console.log('Error: ' + data)
            });
        }
    };

    var isEmail = function(n) {
        var numStr = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
        return numStr.test( n.toString() );
    };
});
