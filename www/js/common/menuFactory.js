/**
 * Created by Prasad on 5/31/2015.
 */
clientModule.factory('menuFactory', function($http, Common) {
    return{
        getFavouriteList: function(){
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/favourite/list'
            })
        },
        feedback: function(paramObj){
            return $http({
                method: 'POST',
                url: Common.BASE_URL + 'api/v1/feedback',
                data: paramObj
            })
        },
        setProfileDetails: function(paramObj){
            return $http({
                method: 'POST',
                url: Common.BASE_URL + 'api/v1/set-profile',
                data: paramObj
            })
        },
        getProfileDetails: function(){
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/get-profile'
            })
        }
    }
});