/**
 * Created by karthik on 5/10/15.
 */
Array.prototype.remove = function () {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

commonModule.factory('Common', function ($ionicLoading, $cordovaToast, $log) {
    return {
        showLoader: function () {
            $ionicLoading.show({
                content: 'Loading',
                animation: 'fade-in',
                showBackdrop: true,
                maxWidth: 200,
                showDelay: 0
            });
        },
        splitString: function(string) {
            if (string != undefined) {
                return  string.split(',');
            } else {
                return ['Unavailable'];
            }
        },
        hideLoader: function () {
            $ionicLoading.hide();
        },
        containsObject: function (obj, list) {
            var i;
            for (i = 0; i < list.length; i++) {
                if (list[i] === obj) {
                    return true;
                }
            }
            return false;
        },
        showToast: function(message) {
            if (window.cordova != undefined) {
                $cordovaToast.showLongBottom(message);
            } else {
                $log.info(message);
            }
        },
        BASE_URL: 'http://api.brewfer.com/',
        //BASE_URL: 'http://192.168.1.55/',
        //BASE_URL: 'http://localhost:8000/',
        GOOGLE_API_KEY: "AIzaSyBCQqknyaMcVGbVQvb60GAIbFxC2Ka0Va4",

        DEFAULT_CITY: 'Mumbai',
        DEFAULT_STATE: 'Maharashtra',
        DEFAULT_CITY_ID: 19,

        messages: {
            NORESULTSFOUND: "Sorry, there are no results for your search query",
            NOTARRIVEDCHECKDEFAULT: "We haven't arrived at your city yet. Setting city to ",
            UNABLETOGETLOCATIONSETDEFAULT: "Unable to get your location. Setting city to ",
            TRYTHESE: "There are no results for your search query. Please check out these offers instead.",
            NOFAVOURITES: "You haven't added any favourites yet.",
            SIGNUPTITLE: "DISCOVER OFFERS AROUND YOU",
            SIGNUPSUBTITLE: "It's quick and easy",
            NOCOMMENTS: "There seem to be no comments for this review. Why don\'t you get started?",
            NOREVIEWS: 'There seem to be no reviews for this restaurant. Why don\'t you get started?',
            REVIEWLENGTHERROR: 'Review length should be greater than 10-500 characters.',
            SWEARWORDERROR: 'Behave yourself. Use some nice words instead?',
            REVIEWUPDATED: 'Review has been updated successfully.',
            REVIEWADDED: 'Review has been added successfully.',
            REVIEWDELETED: 'Review has been deleted successfully.',
            INVALIDNUMBER: 'Please enter a valid mobile number',
            INVALIDNAME: 'Please enter a valid name',
            CONTACTADDED: 'Contact saved successfully',
            NOMENUS: 'Update Coming Soon',
            NOLIVEOFFERS: 'There are no live offers currently. Visit Later.',
            NOHAPPYHOURS: 'There are no happy hours currently. Visit Later.',
            MESSAGESENT: 'Message sent successfully.',
            REVIEWNOTAPPROVED: 'Your review is pending admin approval.',
            CLIENTRATED: 'Thank you for rating.',
            FILLDETAILS: 'Please fill the required details',
            NOEMAIL: 'Please enter a valid email',
            ALLREQUIRED: 'Please fill all the details',
            COMMENTLENGTH: 'Comment length should be more than 5 characters',
            COMMENTDELETE: 'Your comment has been deleted successfully.',
            USERNAMELENGTH: 'Username length should be 3-20 characters.',
            PASSWORDLENGTH: 'Password length should be 6-20 characters.',
            SIGNINTORATE: 'Please sign in to rate',
            CANNOTSHOWOFFERSAROUNDYOU: "Sorry, we can't seem to detect your location.",
            NOTARRIVEDYET: "We haven't arrived at your city yet. Stay tuned.",
            LOGINTOUSE: "Please sign in to use this feature."
        },
        paths: {
            PROFILEPICPATH: "http://brewfer.com/"
        }
    }
});
app.directive('ngEnter', function () {
    return function (scope, element, attrs) {
        element.bind("keydown keypress", function (event) {
            if(event.which === 13) {
                if ($scope.$root == undefined || ($scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest')) {
                    scope.$apply(function () {
                        scope.$eval(attrs.ngEnter);
                    });
                }
                event.preventDefault();
            }
        });
    };
});


commonModule.directive('tabsSwipable', ['$ionicGesture', function($ionicGesture){
        //
        // make ionTabs swipable. leftswipe -> nextTab, rightswipe -> prevTab
        // Usage: just add this as an attribute in the ionTabs tag
        // <ion-tabs tabs-swipable> ... </ion-tabs>
        //
        return {
            restrict: 'A',
            require: 'ionTabs',
            link: function(scope, elm, attrs, tabsCtrl){
                var onSwipeLeft = function(){
                    var target = tabsCtrl.selectedIndex() + 1;
                    if(target < tabsCtrl.tabs.length){
                        scope.$apply(tabsCtrl.select(target));
                    }
                };
                var onSwipeRight = function(){
                    var target = tabsCtrl.selectedIndex() - 1;
                    if(target >= 0){
                        scope.$apply(tabsCtrl.select(target));
                    }
                };

                var swipeGesture = $ionicGesture.on('swipeleft', onSwipeLeft, elm).on('swiperight', onSwipeRight);
                scope.$on('$destroy', function() {
                    $ionicGesture.off(swipeGesture, 'swipeleft', onSwipeLeft);
                    $ionicGesture.off(swipeGesture, 'swiperight', onSwipeRight);
                });
            }
        };
    }]);

/* Mixins for underscore */
(function() {
    _.mixin({
        'findByValues': function(collection, property, values) {
            return _.filter(collection, function(item) {
                return _.contains(values, item[property]);
            });
        }
    });
})();
