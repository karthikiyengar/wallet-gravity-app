/**
 * Created by karthik on 5/31/15.
 */
commonModule.controller('MenuController', function ($scope, loginFactory, $state, Common, menuFactory, $ionicLoading, $ionicHistory, $ionicModal, $cordovaToast, searchFactory, $rootScope) {
    $scope.formData = {title: '', msg: ''};
    $scope.favouriteStatus = Common.messages.NOFAVOURITES;
    $rootScope.guestMode = localStorage.getItem('guest_mode') || 'true';
    $ionicHistory.nextViewOptions({
        disableBack: true
    });

    $scope.getUserName = function() {
        return localStorage.getItem('username');
    };
    $scope.getEmail = function() {
        return localStorage.getItem('email');
    };
    $scope.getProfilePic = function() {
        var profilePic = localStorage.getItem('profilepic');
        if (profilePic == undefined || profilePic == '') {
            return 'img/common/no-pic.jpg';
        } else if (profilePic.indexOf('profilepic') > -1) {
            return Common.paths.PROFILEPICPATH + profilePic;
        } else {
            return profilePic;
        }
    };

    $scope.logout = function () {
        Common.showLoader();
        loginFactory.logout().success(function () {
            localStorage.removeItem('id_token');
            localStorage.removeItem('user_id');
            localStorage.removeItem('email');
            localStorage.removeItem('profilepic');
            localStorage.removeItem('username');
            searchFactory.pageData = {};
            searchFactory.searchData = [];
            searchFactory.formData = {};
            localStorage.setItem('guest_mode', 'true');
            $rootScope.guestMode = localStorage.getItem('guest_mode');
            Common.hideLoader();
            $ionicHistory.nextViewOptions({
                disableBack: true
            });
            $state.go('app.home');
        })
    };

    $scope.openSearch = function() {
        $state.go('app.search');
    };
    $scope.openHome = function() {
        $state.go('app.home');
    };
    $scope.openLogin = function() {
        $state.go('app.login');
    };

    /* Favourite Modal */
    $ionicModal.fromTemplateUrl('templates/favourite.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.favouriteModal = modal;
    });

    $scope.openFavouriteModal = function () {
        $scope.favouriteModal.show();
    };

    $scope.closeFavouriteModal = function () {
        $scope.favouriteModal.hide();
    };

    /* Feedback Modal */
    $ionicModal.fromTemplateUrl('templates/feedback.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.feedbackModal = modal;
    });
    $scope.openFeedbackModal = function () {
        $scope.feedbackModal.show();
    };

    $scope.closeFeedbackModal = function () {
        $scope.formData.feedbacktitle = "";
        $scope.formData.message = "";
        $scope.feedbackModal.hide();
    };

    /* Profile Page Modal */
    $ionicModal.fromTemplateUrl('templates/profile-page.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.profilePageModal = modal;
    });


    /* About Us Modal */
    $ionicModal.fromTemplateUrl('templates/aboutUs.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.aboutModal = modal;
    });

    $scope.openAboutModal = function () {
        $scope.aboutModal.show();
    };

    $scope.closeAboutModal = function () {
        $scope.aboutModal.hide();
    };

    $scope.openProfilePageModal = function () {
        $scope.profileFormData = {};
        $scope.showSave = false;
        /*$scope.profileFormData.email = localStorage.getItem('email');
        $scope.profileFormData.username = localStorage.getItem('username');*/
        Common.showLoader();
        menuFactory.getProfileDetails().success(function(data) {
            $scope.profileFormData.email = data["email"];
            $scope.profileFormData.username = data.name;
            $scope.profileFormData.mobileNumber = data.phone;
            $scope.profileFormData.aboutMe = data.about;
            localStorage.setItem('profilepic', data.uimg);
            Common.hideLoader();
            $scope.profilePageModal.show();
        });

    };

    $scope.closeProfilePageModal = function () {
        $scope.profilePageModal.hide();
    };

    $scope.feedback = function () {
        if ($scope.formData.feedbacktitle == null || $scope.formData.feedbacktitle == '') {
            Common.hideLoader();
            $cordovaToast
                .showLongBottom('Please enter the Title.');
        }
        else if ($scope.formData.message == null || $scope.formData.message == '') {
            Common.hideLoader();
            $cordovaToast
                .showLongBottom('Please enter the Message.');
        }
        else {
            var feedbackObj = {"title": $scope.formData.feedbacktitle, "msg": $scope.formData.message};
            Common.showLoader();
            console.log(feedbackObj);
            menuFactory.feedback(feedbackObj).success(function (data) {
                Common.hideLoader();
                console.log('Response from server:' + data);
                $cordovaToast
                    .showLongBottom(data);
                $scope.closeFeedbackModal();
                $state.go('app.search');
            })
            .error(function (data) {
                console.log('Error: ' + data)
            });
        }
    };

    $scope.listFavourite = function () {
        Common.showLoader();
        menuFactory.getFavouriteList().success(function (data) {
            $scope.favouriteData = data;
            console.log(data);
            Common.hideLoader();
        })
    };

    $scope.formChanged = function() {
        $scope.showSave = true;
    };

    $scope.saveProfileData = function() {
        if ($scope.profileFormData.mobileNumber != undefined && $scope.profileFormData.mobileNumber != '' && ($scope.profileFormData.mobileNumber.toString().length != 10 || !isANumber($scope.profileFormData.mobileNumber))) {
            Common.showToast(Common.messages.INVALIDNUMBER);
            return;
        }
        if ($scope.profileFormData.username == undefined || $scope.profileFormData.username == '' || !/^[a-zA-Z\s]+$/.test($scope.profileFormData.username)) {
            Common.showToast(Common.messages.INVALIDNAME);
            return;
        }

        var profileObj = {
                            "mobileNumber": $scope.profileFormData.mobileNumber,
                            "aboutMe": $scope.profileFormData.aboutMe,
                            "name": $scope.profileFormData.username
                         };
        Common.showLoader();
        console.log(profileObj);
        menuFactory.setProfileDetails(profileObj).success(function (data) {
            Common.hideLoader();
            console.log('Response from server:' + data);
            localStorage.setItem('username', data.name);
            localStorage.setItem('profilepic', data.uimg);
            $scope.closeProfilePageModal();
            Common.showToast('Saved Successfully');
        })
        .error(function (data) {
            console.log('Error: ' + data)
        });
    };

    $ionicModal.fromTemplateUrl('templates/terms.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.termsModal = modal;
    });
    $scope.openTermsModal = function () {
        $scope.termsModal.show();
    };

    $scope.closeTermsModal = function () {
        $scope.termsModal.hide();
    };

    var isANumber = function(n) {
        var numStr = /^-?\d*?\d+$/;
        return numStr.test( n.toString() );
    };
});