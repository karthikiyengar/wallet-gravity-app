/**
 * Created by karthik on 9/27/15.
 */
searchModule.factory('locationFactory', function(Common, searchFactory, $ionicPopup) {
    var cancelledLocation = false;
    var currentLatitude, currentLongitude, cityData, cityPromise;
    (function() {
        cityPromise = searchFactory.getCityList();
        cityPromise.then(function(data){
            cityData = data.data;
            console.log(JSON.stringify(cityData));
        });
    })();


    var showConfirm = function() {
         var confirmPopup = $ionicPopup.confirm({
            title: 'Enable Location?',
            template: 'Do you want to enable your GPS to find the best offers around you?'
        });
        confirmPopup.then(function(res) {
            if(res) {
                console.log('You are sure');
                cordova.plugins.diagnostic.switchToLocationSettings();
                getPosition();
                cancelledLocation = true;
            } else {
                cancelledLocation = true;
            }
        });
    };

    var doNotSetLocation;
    /**
     * Sets up GPS coordinates on app launch.
     * @param openGpsSettings - When set to true, will prompt user for changing GPS Settings
     * @param doNotSetLocation
     */
    var setupGps = function(openGpsSettings, paramDoNotSetLocation) {
        doNotSetLocation = paramDoNotSetLocation != undefined;

        if (window.cordova && window.cordova.plugins.diagnostic) {
            console.log('In setup');
            cordova.plugins.diagnostic.isLocationEnabled(function(enabled) {
                if (openGpsSettings && !enabled && ionic.Platform.isAndroid()) {
                    if (!cancelledLocation) {
                        showConfirm();
                    }
                } else if (enabled) {
                    getPosition();
                } else if (!enabled) {
                    if (!doNotSetLocation) {
                        Common.showToast(Common.messages.UNABLETOGETLOCATIONSETDEFAULT + Common.DEFAULT_CITY);
                        setDefaultLocation();
                    }
                }
            }, function(error) {
                console.log("Error in getting GPS Status: " + error);
            });
        }
    };

    var getPosition = function() {
        /* Try with high accuracy first, and if it fails fall back to low accuracy */
        console.log("getPosition(): Trying with high accuracy");
        navigator.geolocation.getCurrentPosition(successCallbackGetPosition, errorCallbackForGetPosition, { timeout: 10000, enableHighAccuracy: true })
    };

    var successCallbackGetPosition = function(position) {
        setPosition(position.coords.latitude, position.coords.longitude);
    };

    var errorCallbackForGetPosition = function(error) {
        console.log('getPosition() failed: ' + JSON.stringify(error));
        /* If it has timed out, tries low accuracy */
        if (error.code == error.TIMEOUT) {
            navigator.geolocation.getCurrentPosition(
                successCallbackGetPosition, setDefaultLocation, {
                    timeout: 10000,
                    enableHighAccuracy: false
                });
        } else {
            Common.showToast(Common.messages.UNABLETOGETLOCATIONSETDEFAULT + Common.DEFAULT_CITY);
            setDefaultLocation(error);
        }
    };


    var setDefaultLocation = function() {
        if (localStorage.getItem('guest_mode') == 'true') {
            localStorage.setItem('city', Common.DEFAULT_CITY);
            localStorage.setItem('city_id', Common.DEFAULT_CITY_ID);
            localStorage.setItem('default_city_set', true);
        } else {
            searchFactory.setCity({
                'city': Common.DEFAULT_CITY,
                'state': Common.DEFAULT_STATE
            }).success(function () {
                localStorage.setItem('city', Common.DEFAULT_CITY);
                localStorage.setItem('city_id', Common.DEFAULT_CITY_ID);
            })
        }
        searchFactory.refreshAutocomplete();
    };

    var setUserCity = function(city, state, country) {
        localStorage.setItem('current_city', city);
        //Common.showToast("Got city as " + JSON.stringify(city));
        cityPromise.then(function(data) {
            var cityData = data.data || [];
            for (var i = 0; i < cityData.length; i++) {
                if (cityData[i].cyname == city) {
                    if (localStorage.getItem('city') != city) {
                        Common.showToast("Detected your current city as " + city);
                    }
                    localStorage.setItem('city', city);
                    localStorage.setItem('city_id', cityData[i].cyid);
                    localStorage.removeItem('default_city_set');

                    // call refresh for home screen autocomplete
                    searchFactory.refreshAutocomplete();
                    break;
                }
            }
            if (localStorage.getItem('city_id') == undefined) {
                Common.showToast(Common.messages.NOTARRIVEDCHECKDEFAULT + Common.DEFAULT_CITY);
                setDefaultLocation();
            }
        });
        cityPromise.error(function(data) {
            alert(JSON.stringify(data));
        });
    };


    var setPosition = function(latitude, longitude) {
        //Common.showToast("Successfully got location: " + latitude + " " + longitude);
        currentLatitude = latitude;
        currentLongitude = longitude;
        localStorage.setItem('latitude', currentLatitude);
        localStorage.setItem('longitude', currentLongitude);
        if (!doNotSetLocation) {
            getCityFromLatLong(currentLatitude, currentLongitude, setUserCity);
        }
    };


    /***
     * Get city name from latitude and longitude and call function passed when successful
     * @param lat
     * @param long
     */
    var getCityFromLatLong = function(lat, long) {
        var geocoder = new google.maps.Geocoder();
        var latlng = new google.maps.LatLng(lat, long);

        geocoder.geocode({
            'latLng': latlng
        }, geocoderSuccessCB)
    };

    var geocoderSuccessCB = function (results, status) {
        /* If Geocoder is working */
        if (status == google.maps.GeocoderStatus.OK) {
            if (results[1]) {
                for (var i = 0; i < results[0].address_components.length; i++) {
                    for (var b = 0; b < results[0].address_components[i].types.length; b++) {
                        if (results[0].address_components[i].types[b] == "locality") {
                            city = results[0].address_components[i];
                        }
                        if (results[0].address_components[i].types[b] == "administrative_area_level_1") {
                            state = results[0].address_components[i];
                            break;
                        }
                        if (results[0].address_components[i].types[b] == "country") {
                            country = results[0].address_components[i];
                            break;
                        }
                    }
                }
                console.log(city.long_name + " " + state.long_name + " " + country.long_name);
            } else {
                console.log("No results found");
            }
            setUserCity(city.long_name, state.long_name, country.long_name);
        } else {
            Common.showToast(Common.messages.UNABLETOGETLOCATIONSETDEFAULT + Common.DEFAULT_CITY);
            setDefaultLocation();
        }
    };
    return {
        setupGps: setupGps,
        currentLatitude: currentLatitude,
        currentLongitude: currentLongitude,
        getCityFromLatLong: getCityFromLatLong
    }
});