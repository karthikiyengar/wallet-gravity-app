searchModule.controller('HomeController', function ($scope, locationFactory, $ionicPlatform, $http, Common, searchFactory, $state, $rootScope, $filter, $q, $timeout, searchFactory) {
    window._ = _;
    window.$scope = $scope;
    window.$rootScope = $rootScope;
    $rootScope.fromHomePage = true;
    $scope.searchFactory = searchFactory;

    // Set number of items for autocomplete
    var take = 10;

    var localBankName = localStorage.getItem('bank');
    var localBankArray;
    if (localBankName != undefined) {
        localBankArray = localBankName.split(',');
    }

    if (localStorage.getItem('id_token') == undefined) {
        localStorage.setItem('guest_mode', true);
    }

    $scope.homeFormData = {};
    $ionicPlatform.ready(function () {
        if (window.cordova != undefined) {
            setTimeout(function () {
                navigator.splashscreen.hide();
            }, 500);
            locationFactory.setupGps(true);
        }
    });

    $scope.$on('$ionicView.enter', function() {
        locationFactory.setupGps(false, true);
        searchFactory.refreshAutocomplete();
    });

        $scope.getAutocompleteArea = function (query) {
        return $http({
            method: 'GET',
            url: Common.BASE_URL + 'api/v1/autocomplete',
            params: {
                take: take,
                area: query,
                city_id: (localStorage.getItem('city_id') || Common.DEFAULT_CITY_ID)
            }
        }).then(function (response) {
            return response.data;
        });
    };

    $scope.getAutocompleteClientArea = function (query) {
        return $http({
            method: 'GET',
            url: Common.BASE_URL + 'api/v1/autocomplete',
            params: {
                take: take,
                client_or_area: query,
                city_id: (localStorage.getItem('city_id') || Common.DEFAULT_CITY_ID)
            }
        }).then(function(response) {
            return response.data;
        });
    };


    $scope.getAutocompleteCuisine = function (query) {
        return $http({
            method: 'GET',
            url: Common.BASE_URL + 'api/v1/autocomplete',
            params: {
                take: take,
                cuisine: query,
                city_id: (localStorage.getItem('city_id') || Common.DEFAULT_CITY_ID)
            }
        }).then(function (response) {
            return response.data;
        });
    };

    $scope.getAutocompleteBanks = function (query) {
        return $http({
            method: 'GET',
            url: Common.BASE_URL + 'api/v1/autocomplete',
            params: {
                bank: query,
                city_id: (localStorage.getItem('city_id') || Common.DEFAULT_CITY_ID)
            }
        }).then(function (response) {
            return response.data;
        });
    };


    $scope.offersAroundYou = function () {
        if (localStorage.getItem('latitude') == undefined || localStorage.getItem('longitude') == undefined) {
            Common.showToast(Common.messages.CANNOTSHOWOFFERSAROUNDYOU);
        } else {
            $state.go('app.offersaroundyou')
        }
    };

    $scope.homeFormData.bank = [];
    if (localBankName != undefined) {
        $scope.hideInput = true;
        localBankArray = localBankName.split(',');
        _.each(localBankArray, function(value) {
            $scope.homeFormData.bank.push(value);
        });
        $timeout(function() {
            if (localBankArray.length > 1) {
                $scope.homeFormData.bank = localBankArray.length + ' banks selected';
            }
            $scope.hideInput = false;
        }, 50);

    }

    $scope.modelToItemMethod = function (modelValue) {
        return {
            bank: modelValue
        }
    };


    
    $scope.searchRestaurants = function () {
        Common.showLoader();

        // set localStorage city and city_id to the default ones if no city is selected at this point
        if (localStorage.getItem('city') == undefined || localStorage.getItem('city_id') == undefined) {
            localStorage.setItem('city', Common.DEFAULT_CITY);
            localStorage.setItem('city_id', Common.DEFAULT_CITY_ID);
        }

        console.log(autocompleteBanks);
        var searchObj = {};

        if (autocompleteBanks != undefined) {
            /* Store searched banks in localStorage */
            if (autocompleteBanks.length > 0) {
                localStorage.setItem('bank', autocompleteBanks);
                searchObj['bank-name'] = autocompleteBanks.join(',');
            }
        }

        if (searchFactory.formData.searchString != undefined) {
            searchObj['area-client'] = searchFactory.formData.searchString;
        }

        if ($scope.homeFormData.cuisine != undefined) {
            searchObj.cuisine = $scope.homeFormData.cuisine
        }

        if ($scope.homeFormData.searchString != undefined) {
            searchObj['area-client'] = $scope.homeFormData.searchString;
        }


        searchFactory.filterJSON = '{}';

        //TODO Clean this up
        $rootScope.guestMode = localStorage.getItem('guest_mode');
        searchObj.city_id = localStorage.getItem('city_id') || Common.DEFAULT_CITY_ID;
        searchFactory.searchHotels(searchObj);
        $state.go('app.search');
    };


    /* For showing count of selected banks in autocomplete */
    var autocompleteBanks;
    $scope.autocompleteBankSelected = function(callback) {
        console.log(callback.selectedItems);
        if (callback.selectedItems.length == 1) {
            $scope.homeFormData.bank = callback.selectedItems;
        } else if (callback.selectedItems.length == 0) {
            localStorage.removeItem('bank');
            localBankName = null;
            $scope.homeFormData.bank = '';
        } else {
            $scope.homeFormData.bank = callback.selectedItems.length + ' banks selected';
        }

        autocompleteBanks = _.pluck(callback.selectedItems, 'bank');
        window.autocompleteBanks = autocompleteBanks;
    };

    $scope.getCurrentCity = function() {
        return localStorage.getItem('city') || Common.DEFAULT_CITY;
    };


    // set the home page flag to true (so that the CityController understands what page the request is coming from
    $scope.goToCities = function() {
        $rootScope.settingCityFromHomePage = true;
        $state.go('app.cities')
    };


     // give the bank autocomplete external-model an array of items to preselect, fetched from the localStorage
     // the model-to-item method will be called for each element
     $scope.preselectedSearchItems = localBankArray;


    // function which refreshed autocomplete prompts (home screen)
    searchFactory.refreshAutocomplete = function() {
        $scope.cuisines = [{}];
        $scope.getAutocompleteCuisine('').then(function (data) {
            $scope.cuisines = data;
        });

        $scope.banks = [{}];
        var getBankList = $scope.getAutocompleteBanks('');
        getBankList.then(function (data) {
            $scope.banks = data;
        });

        $scope.areas = [{}];
        $scope.getAutocompleteArea('').then(function (data) {
            $scope.areas = data;
        });
    };
    searchFactory.refreshAutocomplete();
});