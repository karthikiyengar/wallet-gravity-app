/**
 * Created by karthik on 5/9/15.
 */
searchModule.controller('SearchController', function ($scope, $http, searchFactory, Common, $cordovaGeolocation, $rootScope, $cordovaToast, $state) {
    // Limit for autocomplete
    var take = 10;
    window.$scope = $scope;
    window.$rootScope = $rootScope;
    console.log('In search controller');
    $rootScope.guestMode = localStorage.getItem('guest_mode');

    // Poll for settings from server
    $http.get(Common.BASE_URL + 'auth/settings', {}).success(function(data) {
        console.log(data);
        $rootScope.showLiveOffers = data.showLiveOffers;
        $scope.showLiveOffers = $rootScope.showLiveOffers;
    });

    $scope.searchFactory = searchFactory;

    if (searchFactory.searchData == undefined) {
        searchFactory.searchData = [];
    }
    if ($rootScope.noMoreItemsAvailable == undefined) {
        $rootScope.noMoreItemsAvailable = false;
    }

    $scope.$on('scroll.infiniteScrollComplete', function() {
        if (searchFactory.searchData.length == 0) {
            $scope.status = 'No Results Found';
        } else {
            $scope.status = '';
        }
    });

    $scope.city = localStorage.getItem('city') || Common.DEFAULT_CITY;

    $scope.getAutocompleteItems = function (query) {
        return $http({
            method: 'GET',
            url: Common.BASE_URL + 'api/v1/autocomplete',
            params: {
                take: take,
                client_or_area: query,
                city_id: (localStorage.getItem('city_id') || Common.DEFAULT_CITY_ID)
            }
        }).then(function(response) {
            return response.data;
        });
    };

    $scope.loadMore = function () {
        console.log('Loadmore called: ' + searchFactory.pageData.nextPage);
        /* Do not search if arriving from home page as search will already be in progress */
        if ($rootScope.fromHomePage) {
            $rootScope.fromHomePage = false;
            return;
        }
        searchFactory.search({city_id:localStorage.getItem('city_id')}, searchFactory.pageData.nextPage).success(function (searchData) {
            searchFactory.storeDataFromResponse(searchData);
            angular.forEach(searchData.data, function (value) {
                searchFactory.searchData.push(value);
            });
            $scope.$broadcast('scroll.infiniteScrollComplete');
        });
    };
    $scope.formData = {};


    $scope.autocompleteClicked = function (callback) {
        if (callback.item != undefined) {
            searchFactory.searchHotels({"area-client":callback.item.Restaurant_Name, "city_id": localStorage.getItem('city_id')});
        }
    };

    $scope.splitString = function(string) {
        return Common.splitString(string);
    };

    $scope.goToCities = function() {
        $rootScope.settingCityFromHomePage = false;
        $state.go('app.cities')
    }
});