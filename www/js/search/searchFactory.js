/**
 * Created by karthik on 5/10/15.
 */
searchModule.factory('searchFactory', function ($http, Common, $rootScope, $cordovaToast) {
    return {
        getAutocomplete: function (key) {
            var paramObj = {};
            paramObj[key] = '';
            paramObj.city_id = localStorage.getItem('city_id');
            console.log(paramObj);
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/autocomplete',
                params: paramObj
            })
        },
        getOffersAroundYou: function(latitude, longitude) {
            return $http({
                url: Common.BASE_URL + 'api/v1/search/gps',
                params: {
                    latitude: latitude,
                    longitude: longitude,
                },
                method: 'GET'
            })
        },
        search: function (paramObj, urlLink) {
            return $http({
                method: 'GET',
                url: urlLink || Common.BASE_URL + 'api/v1/search',
                params: paramObj
            })
        },
        pageData: {},
        formData: {},

        getLiveOffers: function () {
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/live-offers'
            })
        },
        getHappyHours: function () {
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/happy-hours'
            })
        },
        getCities: function () {
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/get-cities'
            })
        },
        setCity: function (paramObj) {
            return $http({
                method: 'POST',
                data: paramObj,
                url: Common.BASE_URL + 'api/v1/set-city'
            })
        },
        storeDataFromResponse: function (searchData) {
            console.log('In store data');
            this.pageData.currentPage = searchData['current_page'];
            this.pageData.nextPage = searchData['next_page_url'] != null ? searchData['next_page_url'].replace('search/?', 'search?') : searchData['next_page_url'];
            console.log(searchData);
            $rootScope.noMoreItemsAvailable = this.pageData.nextPage == null;
        },
        filterJSON: '{}',
        getCityList: function() {
            return $http({
                method: 'GET',
                url: Common.BASE_URL + 'api/v1/get-city-list'
            })
        },
        searchHotels: function (searchObj) {
            Common.showLoader();
            var self = this;
            console.log('Search hotels called');
            console.log(searchObj);
            this.search(searchObj).success(function (searchData) {

                self.storeDataFromResponse(searchData);
                self.searchData = searchData.data;
                $scope.$broadcast('scroll.infiniteScrollComplete');

                if (window.cordova && window.cordova.plugins.Keyboard) {
                    cordova.plugins.Keyboard.close();
                }
                if (Object.keys(self.searchData).length == 0) {
                    $rootScope.status = Common.messages.NORESULTSFOUND;
                    if (Object.keys(searchObj).length > 0) {
                        //TODO Find alternative logic
                        if (searchObj.bank != undefined) {
                            delete searchObj['cost'];
                            delete searchObj['cuisine'];
                            delete searchObj['dinein'];
                            delete searchObj['hd'];
                            delete searchObj['aircond'];
                            delete searchObj['svegnn'];
                            delete searchObj['bar'];
                            delete searchObj['area-client'];
                            Common.showToast(Common.messages.TRYTHESE);
                            self.searchHotels(searchObj);
                        }
                    }
                } else {
                    $rootScope.status = '';
                }
                Common.hideLoader();
            })
        }
    }
})
;