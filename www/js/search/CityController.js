/**
 * Created by Prasad on 13/06/15.
 */

searchModule.controller('CityController', function($scope, $http, searchFactory, Common, $state, $rootScope) {
    console.log('In city Controller');
    Common.showLoader();
    $scope.cityData = {};
    searchFactory.getCities().success(function(cityData){
        console.log(cityData);
        $scope.cityData = cityData;
        Common.hideLoader();
    }).error(function(data){
        console.log(data);
    });

    $scope.setCity = function(city,state,cyid) {
        Common.showLoader();
        console.log(city);
        console.log('Setting city');
        if(localStorage.getItem("guest_mode") == 'false') {
            searchFactory.setCity({ 'city': city, 'state': state }).success(function () { });
        }
        localStorage.setItem('city', city);
        localStorage.setItem('city_id', cyid);
        $scope.city = city;
        Common.hideLoader();
        searchFactory.pageData = {};
        searchFactory.formData = {};
        searchFactory.searchData = [];
        searchFactory.filterJSON = '{}';
        if (!$rootScope.settingCityFromHomePage) {
            $rootScope.noMoreItemsAvailable = false;
            $state.go('app.search');
        } else {
            $state.go('app.home');
        }
    };
    
  $scope.toggleGroup = function(group) {
    if ($scope.isGroupShown(group)) {
      $scope.shownGroup = null;
    } else {
      $scope.shownGroup = group;
    }
  };
  $scope.isGroupShown = function(group) {
    return $scope.shownGroup === group;
  };
});
