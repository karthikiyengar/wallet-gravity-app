/**
 * Created by karthik on 5/10/15.
 */

//TODO: MODAL CLEANUP!!!
//TODO: Cleanup stupid logic of filters and watch. refresh = 1, like really?

searchModule.controller('FilterController', function($scope, $http, searchFactory, $q, $state, Common, $rootScope, $ionicModal, $timeout, $filter) {
    console.log('In filter controller: ' + JSON.stringify(searchFactory.filterJSON));
    window.$scope = $scope;
    window.searchFactory = searchFactory;
    $scope.searchFactory = searchFactory;
    price = {};
    $scope.priceSlider = {
        "min": 0,
        "max": 10000,
        "ceil": 10000,
        "floor": 0
    };


    $scope.translate = function(value)
    {
        return '₹' + value;
    };

    $scope.sliderLimitChanged = function () {
        $scope.showApply = true;
    };

    $scope.sortOrder = 'Descending';
    $scope.showOrder = false;
    var filterObj = JSON.parse(searchFactory.filterJSON);
    window.filterObj = filterObj;

    $scope.allBanks = filterObj.allBanks || false ;

    var localBankId = parseInt(localStorage.getItem('bank_id'));
    var localBankName = localStorage.getItem('bank');
    var localCityId = parseInt(localStorage.getItem('city_id'));

    $scope.$watch('filterObj', function(newVal){
        console.log('In Watch');
        if (filterObj.order == 'asc') {
            $scope.sortOrder = 'Ascending';
            $scope.showOrder = true;
        } else if (filterObj.sort != undefined) {
            $scope.sortOrder = 'Descending';
            $scope.showOrder = true;
        }
        /**
         If a bank/cuisine has been selected from the more button, add it to the lessBanks/lessCuisine array
         so that it is visible on the UI.
         */
        if (filterObj.bank != undefined && (filterObj.allBanks == false || filterObj.allBanks == undefined)) {
            angular.forEach(filterObj.bank, function (bankValue, key, object) {
                angular.forEach($scope.banks, function (bankObject, key, object) {
                    if (bankObject.bank_id == bankValue) {

                        $scope.pushIntoLessBanks(bankObject);
                    }
                });
            });
        }

        if (filterObj.cuisine != undefined) {
            angular.forEach(filterObj.cuisine, function (cuisineValue, key, object) {
                angular.forEach($scope.cuisines, function (cuisineObject, key, object) {
                    if (cuisineObject.cousine == cuisineValue) {
                        $scope.pushIntoLessCuisines(cuisineObject);

                    }
                });
            });
        }
        $scope.showApply = Object.keys(newVal).length > 0;
    }, true);
    Common.showLoader();


    $scope.toggleAllBanks = function() {
        $scope.allBanks = !$scope.allBanks;
        if ($scope.allBanks) {
            if(filterObj['bank'] == undefined) {
                filterObj.bank = [];
            }
            angular.forEach($scope.banks, function(value, key, object) {
                if (filterObj.bank.indexOf(value['bank_id']) == -1) {
                    filterObj.bank.push(value['bank_id']);
                }
            });
            filterObj.allBanks = true;
            $scope.lessBanks = [];
        } else {
            filterObj.allBanks = false;
            filterObj.bank = [];
            $scope.lessBanks = [];
            angular.forEach($scope.banks, function (value, key, obj) {
                if (key < 4) {
                    $scope.lessBanks.push(value);
                }
            });
        }
        console.log(JSON.stringify(filterObj.bank));
    };

    var requests = {};

    /**
     * AJAX call to get cuisines.
     * @returns {*}
     */
    requests.cuisineRequest = function() {
        return searchFactory.getAutocomplete('cuisine').success(function (data) {
            $scope.cuisines = data;
            $scope.lessCuisines = [];
            angular.forEach($scope.cuisines, function (value, key, obj) {
                if (key < 4) {
                    $scope.lessCuisines.push(value);
                }
            });

        }).error(function (data) {
            console.log('Error in getting cuisines: ' + JSON.stringify(data));
        });
    };

    /**
     * lessCuisines is what is rendered on the filters page when it is loaded. It contains the top 5 cuisines and all
     * the other selected cuisines.
     * @param cuisine
     */
    $scope.pushIntoLessCuisines = function(cuisine) {
        if (!Common.containsObject(cuisine, $scope.lessCuisines)) {
            $scope.lessCuisines.push(cuisine);
        }
    };

    /**
     * AJAX call to get banks.
     * @returns {*}
     */
    requests.bankRequest = function() {
        return searchFactory.getAutocomplete('bank').success(function (data) {
            //console.log('Ionic: ' + JSON.stringify(data));
            $scope.banks = data;
            $scope.lessBanks = [];
            if (!$scope.allBanks) {
                angular.forEach($scope.banks, function (value, key, obj) {
                    if (key < 4) {
                        $scope.lessBanks.push(value);
                    }
                });
            }
        }).error(function (data) {
            console.log('Error in getting cuisines: ' + JSON.stringify(data));
        });
    };

    $scope.pushIntoLessBanks = function(bank) {
        $scope.allBanks = $scope.filterObj.bank.length == $scope.banks.length;
        if (!Common.containsObject(bank, $scope.lessBanks)) {
            $scope.lessBanks.push(bank);
        }
    };

    //$scope.banks = [{"bank_id":40,"bank":"YES","status":"1","cityid":1},{"bank_id":39,"bank":"HSBC","status":"1","cityid":1},{"bank_id":38,"bank":"AXIS","status":"1","cityid":1},{"bank_id":37,"bank":"AMEX","status":"1","cityid":1},{"bank_id":36,"bank":"KOTAK","status":"1","cityid":1}];
    //$scope.cuisines = [{"cousine":"Desserts"},{"cousine":"Bakery"},{"cousine":"European"},{"cousine":"Italian"},{"cousine":"Mexican"},{"cousine":"Biryani"},{"cousine":"Chinese"},{"cousine":"North Indian"}];


    /**
     * Generic function to add clicked filters to an object which will be sent as GET Parameter
     * @param object - Object of the filter element: E.g {"bank_id":40,"bank":"YES","status":"1","cityid":1}
     * @param filterName - The name of Get parameter
     * @param item - The property name of the object to be sent in the GET request. E.g: bank, Restaurant_Name
     * TODO Can be heavily optimized/better written. To whoever is reading, good luck.
     */

    $scope.toggleFilter = function(object, filterName, item, multiselect) {
        if (!object.selected) {
            if (filterObj[filterName] == undefined) {
                filterObj[filterName] = [];
            }
            else if (filterObj[filterName].indexOf(item) > -1) {
                filterObj[filterName].remove(item);
                object.selected = false;
                if (filterObj[filterName].length == 0) {
                    delete filterObj[filterName];
                }
                $scope.filterObj = filterObj;
                return;
            }
            if (multiselect) {
                filterObj[filterName].push(item);
                object.selected = true;
            }
            else {
                filterObj[filterName].pop();
                filterObj[filterName].push(item);
                object.selected = true;
            }
        }
        else {
            if (filterObj[filterName] != undefined && filterObj[filterName].indexOf(item) > -1) {
                filterObj[filterName].remove(item);
                object.selected = false;
                if (filterObj[filterName] != undefined && filterObj[filterName].length == 0) {
                    delete filterObj[filterName];
                }
            } else {
                object.selected = false;
                $scope.toggleFilter(object,filterName,item,multiselect);
            }
        }
        $scope.filterObj = filterObj;
    };

    $scope.clearFilters = function() {
        /* Ugly hack? */
        $scope.allBanks = true;
        $scope.toggleAllBanks();
        filterObj.allBanks = false;
        $scope.priceSlider.min = $scope.priceSlider.floor;
        $scope.priceSlider.max = $scope.priceSlider.ceil;
        filterObj = {};
        $scope.filterObj = {};
        $scope.showApply = false;
    };

    /**
     * Toggles flags/sorting and adds/removes them from filterObj
     * @param item
     * @param value
     * @param multiselect
     */
    $scope.toggleFlagOrSort = function(item, value, multiselect) {
        if (item == 'order') {
            if ($scope.sortOrder == 'Descending') {
                value = 'asc';
                $scope.sortOrder = 'Ascending';
            }
            else if ($scope.sortOrder == 'Ascending') {
                value = 'desc';
                $scope.sortOrder = 'Descending';
            }
        }
        if (typeof(filterObj[item]) == 'undefined') {
            filterObj[item] = value;
            filterObj['order'] = 'desc';
            $scope.sortOrder = 'Descending';
            $scope.showOrder = true;
        }
        else if (!multiselect) {
            if (filterObj[item] == value) {
                delete filterObj[item];
                delete filterObj["order"];
                $scope.showOrder = false;
            }
            else {
                filterObj[item] = value;
            }
        }
        else {
            delete filterObj[item];
        }
        $scope.filterObj = filterObj;
    };

    /**
     * Returns true if selected and false if not. Checks filterObj to see whether the element is present
     * @param filterName - Array in the filterObj to scan.
     * @param item - Value to search in the array
     * @returns {boolean}
     */
    $scope.getFilterCSS = function(filterName, item) {
        if (filterObj[filterName] != undefined) {
            return filterObj[filterName].indexOf(item) != -1;
        }
    };

    /**
     * CSS Handler for flags
     * @param filterName
     * @returns {boolean}
     */
    $scope.getFlagOrSortCSS = function(filterName, item) {
        if (typeof(filterObj[filterName]) != 'undefined') {
            return filterObj[filterName] == item;
        }
    };

    $scope.applyFilters = function() {

        /* Set price slider values before the filter is applied */
        if ($scope.priceSlider.min == $scope.priceSlider.floor && $scope.priceSlider.max == $scope.priceSlider.ceil) {
            filterObj.cost = [];
        } else {
            filterObj.cost = [$scope.priceSlider.min, $scope.priceSlider.max];
        }

        /* Stringify the values and store it in the factory - Have done this to save filter settings once applied */
        searchFactory.filterJSON = JSON.stringify(filterObj);

        for (var index in filterObj) {
            if (typeof(filterObj[index]) == 'object') {
                filterObj[index] = filterObj[index].join(',');
            }
        }
        if (searchFactory.formData.searchString != '')
        filterObj["area-client"] = searchFactory.formData.searchString;
        filterObj["city_id"] = localStorage.getItem('city_id');
        $rootScope.noMoreItemsAvailable = true;
        searchFactory.searchData = [];
        searchFactory.searchHotels(filterObj);
        $state.go('app.search');
    };


    /* Wait until all AJAX Promises complete and then show the page */

    $q.all([requests.cuisineRequest(), requests.bankRequest()]).then(function() {
        $scope.filtersLoaded = true;
        $timeout(function(){
            $scope.$broadcast('reCalcViewDimensions');
        }, 10);
        console.log('Q loaded. Refreshing filterObj');
        filterObj = JSON.parse(searchFactory.filterJSON);


        /**
         * Dirty way to refresh less array. Figure out a way to get the watch to trigger as soon as parse
         */
        angular.forEach(filterObj.cuisine, function (cuisineValue, key, object) {
            angular.forEach($scope.cuisines, function (cuisineObject, key, object) {
                if (cuisineObject.cousine == cuisineValue) {
                    $scope.pushIntoLessCuisines(cuisineObject);
                }
            });
        });
        if (filterObj.bank != undefined && (filterObj.allBanks == false || filterObj.allBanks == undefined)) {
            angular.forEach(filterObj.bank, function (bankValue, key, object) {
                angular.forEach($scope.banks, function (bankObject, key, object) {
                    if (bankObject.bank_id == bankValue) {
                        $scope.pushIntoLessBanks(bankObject);
                    }
                });
            });
        }
        if(filterObj.cost != undefined && filterObj.cost.length != 0){
            $scope.priceSlider.min = filterObj.cost[0];
            $scope.priceSlider.max = filterObj.cost[1];
        } else {
            $scope.priceSlider.min = $scope.priceSlider.floor;
            $scope.priceSlider.max = $scope.priceSlider.ceil;
        }

        /**
         * Add bank from localStorage (Preselect)
         */
        if (localBankId != undefined && localBankName != undefined) {
            if (filterObj.bank == undefined || (filterObj.bank != undefined && filterObj.bank.indexOf(localBankId) == -1)) {
                var bank = {};
                //console.log($scope.banks);
                var splitBanks;
                if (localBankName != undefined) {
                    splitBanks = localBankName.split(',');
                }
                var foundBanks = _.findByValues($scope.banks, "bank", splitBanks);
                _.each(foundBanks, function(value) {
                    $scope.toggleFilter(value, 'bank', value.bank_id, true);
                });
            }
        }

        Common.hideLoader();
    });


    /* Modals */
    $ionicModal.fromTemplateUrl('bank-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.bankModal = modal
    });

    $scope.openBankModal = function () {
        $scope.bankModal.show()
    };

    $scope.closeBankModal = function () {
        $scope.bankModal.hide();
    };

    /* Modals */
    $ionicModal.fromTemplateUrl('cuisines-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.cuisineModal = modal
    });

    $scope.openCuisineModal = function () {
        $scope.cuisineModal.show()
    };

    $scope.closeCuisineModal = function () {
        $scope.cuisineModal.hide();
    };

    $scope.filterObj = filterObj;

});