/**
 * Created by karthik on 5/9/15.
 */
var commonModule = angular.module('starter.common', []);
var app = angular.module('starter.controllers',['starter.common']);
var loginModule = angular.module('starter.login', ['starter.common']);
var searchModule = angular.module('starter.search', ['starter.common']);
var clientModule = angular.module('starter.client', ['starter.common']);

