/**
 * Created by Prasad on 5/28/2015.
 */
searchModule.controller('HappyHoursController', function($scope, $http, searchFactory, Common) {
    console.log('In Discount Controller');
    $scope.status = Common.messages.NOHAPPYHOURS;
        Common.showLoader();
        searchFactory.getHappyHours().success(function(data){
            $scope.offersData = data;
            console.log(data);
            Common.hideLoader();
        }).error(function(data){
            console.log(data);
        })
});