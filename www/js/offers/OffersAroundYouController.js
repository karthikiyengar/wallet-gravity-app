/**
 * Created by karthik on 9/29/15.
 */
searchModule.controller('OffersAroundYouController', function($scope, $http, searchFactory, Common) {
    window.$scope = $scope;
    Common.showLoader();
    searchFactory.getOffersAroundYou(localStorage.getItem('latitude'), localStorage.getItem('longitude'))
        .then(function(data) {
            console.log(data);
            $scope.offersAroundYou = data.data;
            if ($scope.offersAroundYou.length == 0) {
                $scope.status = "We couldn't find any offers around your current location. Please try again later"
            }
            Common.hideLoader();
        });


    $scope.splitString = function(string) {
        return Common.splitString(string);
    }
});