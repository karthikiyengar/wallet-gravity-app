/**
 * Created by Prasad on 5/28/2015.
 */
searchModule.controller('LiveOffersController', function($scope, $http, searchFactory, Common) {
    console.log('In Discount Controller');
    $scope.status = Common.messages.NOLIVEOFFERS;
    Common.showLoader();
    searchFactory.getLiveOffers().success(function(data){
        $scope.offersData = data;
        console.log(data);
        Common.hideLoader();
    }).error(function(data){
        console.log(data);
    })
});