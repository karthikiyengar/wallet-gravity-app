// Ionic Starter App
// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.login', 'starter.search', 'starter.client', 'ngCordova', 'ngCordovaOauth', 'angular-jwt', 'ngResource', 'ngGentle', 'ionic.rating','ion-autocomplete','rzModule','uiGmapgoogle-maps', 'ngIOS9UIWebViewPatch'])
    .config(function Config($httpProvider, jwtInterceptorProvider, $ionicConfigProvider) {
        // Global Timeout
        $httpProvider.interceptors.push('timeoutHttpIntercept');

        // Enable native scrolling for Android
        if (!ionic.Platform.isIOS())$ionicConfigProvider.scrolling.jsScrolling(false);

        // Force tabs to the bottom for android
        $ionicConfigProvider.tabs.position('top');

        jwtInterceptorProvider.tokenGetter = ['loginFactory', 'jwtHelper', 'config', function (loginFactory, jwtHelper, config) {
            if (config.url.substr(config.url.length - 5) == '.html') {
                return null;
            } else {
                console.log('Returning Local Storage Token: ' + localStorage.getItem('id_token'));
                return localStorage.getItem('id_token');
            }
        }];

        $httpProvider.interceptors.push('jwtInterceptor');
        $httpProvider.interceptors.push(function () {
            return {
                'responseError': function (response) {
                    if (response.status === 401) {
                        localStorage.removeItem('id_token');
                    }
                }
            };
        });
        console.log(JSON.stringify($httpProvider.interceptors));

        /* Global Errror Handling */

        $httpProvider.interceptors.push(function ($q, $injector) {
            return {
                'response': function (response) {
                    return response;
                },
                'responseError': function (rejection) {
                    $injector.get('$ionicLoading').hide();
                    $injector.get('Common').showToast('Cannot connect to server. Please try later.');
                    $injector.get('$rootScope').noMoreItemsAvailable = true;
                    return $q.reject(rejection);
                }
            };
        });
    })
    .run(function ($ionicPlatform, $ionicPopup) {
        $ionicPlatform.ready(function ($cordovaNetwork) {
            if (ionic.Platform.isIOS()) {
                ionic.Platform.fullScreen();
                cordova.plugins.Keyboard.disableScroll(true);
                StatusBar.hide();
            }

            // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
            // for form inputs)
            if (window.cordova && window.cordova.plugins.Keyboard) {
                cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            }

            // Check for network connection
            if (window.Connection) {
                if (navigator.connection.type == Connection.NONE) {
                    $ionicPopup.confirm({
                        title: 'No Internet Connection',
                        content: 'Sorry, no Internet connectivity detected. Please reconnect and try again.'
                    })
                        .then(function (result) {
                            if (!result) {
                                ionic.Platform.exitApp();
                            }
                        });
                }
            }

        });
    })
    .config(function ($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('app', {
                url: "/app",
                abstract: true,
                controller: 'MenuController',
                templateUrl: "templates/menu.html",
                cache:false
            })
            .state('app.home', {
                url: "/home",
                views: {
                    'menuContent': {
                        templateUrl: "templates/home.html",
                        controller: 'HomeController'
                    }
                },
                cache:true
            })
            .state('app.search', {
                cache: false,
                url: "/search",
                views: {
                    'menuContent': {
                        templateUrl: "templates/search.html",
                        controller: 'SearchController'
                    }
                }
            })
            .state('app.offersaroundyou', {
                cache: false,
                url: "/offersaroundyou",
                views: {
                    'menuContent': {
                        templateUrl: "templates/offersAroundYou.html",
                        controller: 'OffersAroundYouController'
                    }
                }
            })
            .state('app.cities', {
                cache: false,
                url: "/cities",
                views: {
                    'menuContent': {
                        templateUrl: "templates/cities.html",
                        controller: 'CityController'
                    }
                }
            })
            .state('app.liveOffer', {
                cache: false,
                url: "/liveOffer",
                views: {
                    'menuContent': {
                        templateUrl: 'templates/liveOffer.html',
                        controller: 'LiveOffersController'
                    }
                }
            })
            .state('app.happyHour', {
                cache: false,
                url: "/happyHour",
                views: {
                    'menuContent': {
                        templateUrl: 'templates/happyHour.html',
                        controller: 'HappyHoursController'
                    }
                }
            })
            .state('app.filter', {
                cache: false,
                url: "/filter",
                views: {
                    'menuContent': {
                        templateUrl: "templates/filter.html",
                        controller: 'FilterController'
                    }
                }
            })
            .state('app.client', {
                url: "/client/:clientId",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/client.html",
                        controller: 'ClientController'
                    }
                }
            })
            .state('app.review', {
                cache: false,
                url: "/review/:reviewId",
                views: {
                    'menuContent': {
                        templateUrl: "templates/review.html",
                        controller: 'ReviewController'
                    }
                }
            })

            .state('app.login', {
                url: "/login",
                views: {
                    'menuContent': {
                        templateUrl: "templates/login.html",
                        controller: 'LoginController'
                    }
                }
            })
            .state('app.signin', {
                cache: false,
                url: "/signin",
                views: {
                    'menuContent': {
                        templateUrl: "templates/signin.html",
                        controller: 'LoginController'
                    }
                }
            })
            .state('app.resetpassword', {
                url: "/resetpassword",
                views: {
                    'menuContent': {
                        templateUrl: "templates/reset-password.html",
                        controller: 'LoginController'
                    }
                }
            })
            .state('app.register', {
                url: "/register",
                cache: false,
                views: {
                    'menuContent': {
                        templateUrl: "templates/register.html",
                        controller: 'RegisterController'
                    }
                }
            });
        // if none of the above states are matched, use this as the fallback
        $urlRouterProvider.otherwise('app/home');
    }).factory('timeoutHttpIntercept', function ($rootScope, $q) {
        return {
            'request': function(config) {
                config.timeout = 20000;
                return config;
            }
        };
    });