/**
 * Created by karthik on 5/10/15.
 */
clientModule.factory('clientFactory', function($http, Common) {
    return {
        getClient: function(id) {
            return $http({
                method:'GET',
                url: Common.BASE_URL + 'api/v1/client',
                params: {
                    client_id: id
                }
            })
        },
        sendMessage: function(clientId, mobileNumber) {
            return $http({
                method:'POST',
                url: Common.BASE_URL + 'api/v1/sms-details',
                data: {
                    client_id:clientId,
                    mobile_number:mobileNumber
                }
            })
        },
        toggleLike: function(paramObj){
            return $http({
                method:'PUT',
                url:Common.BASE_URL + 'api/v1/like-review',
                data:paramObj
            })
        },
        postRating: function(paramObj) {
            return $http({
                method:'POST',
                url:Common.BASE_URL + 'api/v1/rating',
                data:paramObj
            })
        },
        postFav: function(paramobj){
          return $http({
              method:'POST',
              url:Common.BASE_URL + 'api/v1/favourite',
              data:paramobj
          })
        },
        putReview: function(review, clientId) {
            return $http({
                method:'PUT',
                url: Common.BASE_URL + 'api/v1/review',
                params: {
                    'review': review,
                    'client_id': clientId
                }
            })
        },
        deleteReview: function(reviewId) {
            return $http({
                method:'DELETE',
                url: Common.BASE_URL + 'api/v1/review/' + reviewId
            });
        }
    }
});