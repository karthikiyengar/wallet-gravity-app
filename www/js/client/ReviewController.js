/**
 * Created by karthik on 5/10/15.
 */

clientModule.controller('ReviewController', function($scope, reviewFactory, commentFactory, $stateParams, Common, $ionicLoading, paragraph, $cordovaToast) {
    console.log('In review');
    window.$scope = $scope;
    var selectedCommentId;
    $scope.reviewLoaded = false;
    $scope.commentData = {};
    $scope.notApprovedReviewStatus = Common.messages.REVIEWNOTAPPROVED;
    var toggleFlag = false;
    console.log('Review Controller');

    $scope.editingMode = false;
    userId = localStorage.getItem('user_id');

    $scope.postComment = function() {
        if (localStorage.getItem('guest_mode') == 'true') {
            Common.showToast(Common.messages.LOGINTOUSE);
            return;
        }
        if ($scope.commentData.comment==null || $scope.commentData.comment=='' || $scope.commentData.comment.length < 5) {
            Common.showToast(Common.messages.COMMENTLENGTH);
        }
        else if (paragraph.isGentle($scope.commentData.comment).length > 0) {
            Common.showToast(Common.messages.COMMENTLENGTH);
        }
        else {
            Common.showLoader();
            commentFactory.postComment({review_id: $stateParams.reviewId, comment: $scope.commentData.comment}).success(function(data) {
                console.log('Saved Successfully: ' + JSON.stringify(data));
                $scope.reviewData[0].comments.push(data);
                refreshComments();
                $scope.commentData.comment = '';
                Common.hideLoader();
            });
        }
    };

    $scope.putComment = function() {
        if (localStorage.getItem('guest_mode') == 'true') {
            Common.showToast(Common.messages.LOGINTOUSE);
            return;
        }
        if ($scope.commentData.comment==null || $scope.commentData.comment=='' || $scope.commentData.comment.length < 5) {
            Common.showToast(Common.messages.COMMENTLENGTH);
        }
        else if (paragraph.isGentle($scope.commentData.comment).length > 0) {
            Common.showToast(Common.messages.SWEARWORDERROR);
        }
        else {
            Common.showLoader();
            commentFactory.putComment({review_id: $stateParams.reviewId, comment: $scope.commentData.comment, comment_id: selectedCommentId}).success(function(data) {
                console.log('Saved Successfully: ' + JSON.stringify(data));
                angular.forEach($scope.reviewData[0].comments, function(val, key, obj) {
                    if (val.id == selectedCommentId) {
                        obj.splice(key,1);
                    }
                });
                $scope.editingMode = false;
                $scope.commentData.comment = '';
                console.log(data);
                console.log($scope.reviewData[0].comments);
                $scope.reviewData[0].comments.push(data);
                refreshComments();
                console.log($scope.reviewData[0].comments);
                Common.hideLoader();
            });
        }
    };

    $scope.deleteComment = function(commentId) {
        Common.showLoader();
        commentFactory.deleteComment({comment_id:commentId}).success(function() {
            angular.forEach($scope.reviewData[0].comments, function(val, key, obj) {
                if (val.id === commentId) {
                    obj.splice(key,1);
                }
            });
            Common.hideLoader();
            Common.showToast(Common.messages.COMMENTDELETE);
        });
    };

    
    $scope.toggleLike = function(comment) {
        if (localStorage.getItem('guest_mode') == 'true') {
            Common.showToast(Common.messages.LOGINTOUSE);
            return;
        }
        toggleFlag = true;
        if (comment.hasLike) {
            comment.likeCount = comment.likeCount - 1;
            comment.likeStatus = 'Like';
            comment.hasLike = !comment.hasLike;
        } else {
            comment.likeCount = comment.likeCount + 1;
            comment.likeStatus = 'Unlike';
            comment.hasLike = !comment.hasLike;
        }
        commentFactory.toggleLike({
            'comment_id': comment.id,
            'status': (+ comment.hasLike)
        }).success(function(data) {
            //console.log('Like:' + JSON.stringify(data));
            comment.likes.push(data);
        });
    };

    $scope.$watch('reviewData', function(newVal) {
        if (typeof $scope.reviewData[0] != 'undefined'  && typeof($scope.reviewData[0].comments) != 'undefined') {
            if ($scope.reviewData[0].comments.length === 0) {
                $scope.commentStatus = Common.messages.NOCOMMENTS;
            } else {
                $scope.commentStatus = '';
            }
            refreshComments();
        }
    }, true);


    var refreshComments = function() {
        angular.forEach($scope.reviewData[0].comments, function(value, key, obj) {

            /* Check whether current user has comments */
            if (parseInt(value.uid) === parseInt(userId)) {
                value.hasComment = true;
            }

            /* If the like/unlike is not clicked from the UI, run this loop (initialize likes) */
            if (!toggleFlag || value.likeCount == undefined) {
                value.likeCount = 0;
                angular.forEach(value.likes, function (like) {
                    /* If the like is active (status = 1) */
                    if (parseInt(like.status) === 1) {
                        value.likeCount = value.likeCount + 1;
                        /* Check whether the current user has likes */
                        if (parseInt(like.uid) === parseInt(userId)) {
                            value.hasLike = true;
                        }
                    }
                });
                /* Set others as not liked */
                if (typeof(value.hasLike) === 'undefined') {
                    value.hasLike = false;
                }
                /* Set status message (can be a better way) */
                value.likeStatus = value.hasLike ? 'Unlike' : 'Like';
            }
        });
    };
    Common.showLoader();
    $scope.reviewData = reviewFactory.query({review_id: $stateParams.reviewId }, function() {
        $scope.reviewLoaded = true;
        Common.hideLoader();
    });

    $scope.toggleEdit = function(comment, id) {
        console.log('In toggle');
        selectedCommentId = id;
        $scope.editingMode = !$scope.editingMode;
        if ($scope.editingMode) {
            $scope.commentData.comment = comment;
            $scope.commentData.comment_id = id;
        } else {
            $scope.commentData.comment = '';
        }
    };

    //$scope.reviewData = [{"rid":73,"client_id":"1248","review":"Lovely presentation or i should say .. mind blowing... and yummmy food.. those death wings are killer... awesomosas delicious.. the cheese stuff.. super crazy... best part desserts... the chocolate dessesrt and the ramesh suresh was mind bogling... interiors.. unique antique.. and everything that goes along with it! best place may it be lunch or dinner..\r\n   ","uid":55,"status":1,"user":{"uid":55,"name":"Mayuri Kadam","email":"mayurikadam708@gmail.com","uimg":"http:\/\/graph.facebook.com\/831997993532256\/picture","verify_flag":"1","social_flag":"fb","cityid":0},"likes":[{"id":79,"uid":"72","reviewid":"73","time1":"15-05-05 16:14:21","status":"1"},{"id":80,"uid":"56","reviewid":"73","time1":"15-05-05 15:46:36","status":"1"},{"id":87,"uid":"86","reviewid":"73","time1":"15-05-08 17:28:26","status":"0"},{"id":90,"uid":"2","reviewid":"73","time1":"15-05-10 10:58:51","status":"1"}],"comments":[{"id":45,"uid":"56","reviewid":"73","time1":"15-05-05 15:47:22","comment":"ohh...ill definitely visit....:)","user":{"uid":56,"name":"Bhagyashree Dighe","email":"dighe.bhagyashree@gmail.com","uimg":"http:\/\/graph.facebook.com\/837090143030702\/picture","verify_flag":"1","social_flag":"fb","cityid":0},"likes":[{"id":45,"uid":"2","cid":"47","time1":"15-05-10 11:05:10","status":"1"}]},{"id":47,"uid":"2","reviewid":"73","time1":"15-05-10 11:03:33","comment":"ohhhhhhhhhhhhh","user":{"uid":2,"name":"wallet","email":"wallet@gmail.com","uimg":"","verify_flag":"1","social_flag":"","cityid":1},"likes":[]}]}]
});