clientModule.factory('commentFactory', function($resource, Common, $http) {
    return {
        putComment: function(paramObj) {
            /* Needs comment_id, review_id and comment */
            return $http({
                method:'PUT',
                url:Common.BASE_URL + 'api/v1/comment',
                data: paramObj
            })
        },
        postComment: function(paramObj) {
            return $http({
                method:'POST',
                url:Common.BASE_URL + 'api/v1/comment',
                data: paramObj
            })
        },
        deleteComment: function(paramObj) {
            return $http({
                method:'DELETE',
                url:Common.BASE_URL + 'api/v1/comment',
                params: paramObj
            })
        },
        toggleLike: function(paramObj){
            return $http({
                method:'PUT',
                url:Common.BASE_URL + 'api/v1/like-comment',
                data:paramObj
            })
        }
    };
});