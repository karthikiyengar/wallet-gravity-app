/**
 * Created by karthik on 5/23/15.
 */
clientModule.factory('reviewFactory', function($resource, Common) {
    return $resource(Common.BASE_URL + 'api/v1/review');
});