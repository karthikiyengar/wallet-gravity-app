/**
 * Created by karthik on 5/10/15.
 */
clientModule.controller('ClientController', function ($scope, clientFactory, $stateParams, Common, $ionicModal, $cordovaContacts, $ionicLoading, paragraph, $cordovaToast, $ionicSlideBoxDelegate, $ionicPopup, $timeout) {
    var clientId = $stateParams.clientId;
    var userId = parseInt(localStorage.getItem('user_id'));

    $scope.menuPrefix = "http://brewfer.com/admin/upload_menus/";
    $scope.photoPrefix = "http://brewfer.com/admin/uploads/";
    $scope.bannerPrefix = "http://brewfer.com/admin/banner_img/";

    $scope.editReview = false;
    window.$scope = $scope;
    console.log('User ID: ' + userId);

    $scope.menuStatus = Common.messages.NOMENUS;
    $scope.notSorted = function(obj){
        if (!obj) {
            return [];
        }
        return Object.keys(obj);
    };

    /*$scope.clientData = {"clientDetails":[{"clid":"1868","countryid":"1","stateid":"17","cityid":"1","mainid":"0","submainid":"0","Restaurant_Name":"Ahmed Bhai's Bry & Dry","contact":"022 27714141","email":"tech@sharptechcompany.com","address":"32, Krishnakamal CHS, Opposite Police Station, Sector 21, Nerul,  Vashi, Navi Mumbai","area":"Vashi","hd":"1","dinein":"1","svegnn":"0","bar":"0","st":"0","aircond":"1","cuisine":"Seafood , North Indian , Chinese , Biryani","knownfor":"Making us aware of our palate for seafood","order_sugest":"Null","payment":"Cash And Cards Accepted","bank":"","cost":1300,"isFavourite":true,"votes":8,"rating":"4","avgRating":"4.7","timings":{"Sunday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM","Monday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM","Tuesday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM","Wednesday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM","Thursday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM","Friday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM","Saturday":"12:30 PM to 3:30 PM and 7:30 PM to 12:30 AM"},"openStatus":"Closed","photos":[{"id":"164","client_id":"1868","image":"1.jpg","status":"1"},{"id":"165","client_id":"1868","image":"2.jpg","status":"1"},{"id":"166","client_id":"1868","image":"4.jpg","status":"1"}],"menus":[{"id":"71","client_id":"1868","image":"download.jpg","status":"1"},{"id":"72","client_id":"1868","image":"images.jpg","status":"1"}],"review":[{"rid":"120","client_id":"1868","review":" Went there searching for a new place to have lunch. Chose to go to the shamiana section which is gives of a royal, private feel and is air conditioned as well. Had heard quite a few good reviews so thought why not give it a try. The ambience is ok. Ordered the afghani kebab along with chicken tikka biryani as mains. Honestly the taste exceeded expectations. The serving was good as well as the flavors. Would probably go next time with family. The place is located close to Nerul station so it's an added bonus when travelling from other places .....","timer":"15-06-25 11:06:27","uid":"56","status":"0","user":null,"likes":[{"id":"105","uid":"56","reviewid":"120","time1":"15-06-25 11:06:21","status":"1"},{"id":"107","uid":"151","reviewid":"120","time1":"15-06-18 12:08:09","status":"1"},{"id":"116","uid":"163","reviewid":"120","time1":"15-06-17 10:25:09","status":"1"},{"id":"123","uid":"149","reviewid":"120","time1":"15-06-18 17:12:19","status":"1"},{"id":"126","uid":"143","reviewid":"120","time1":"2015-06-23 22:35:15","status":"1"},{"id":"128","uid":"2","reviewid":"120","time1":"2015-06-26 22:31:42","status":"1"}]},{"rid":"122","client_id":"1868","review":"I heard a lot about it from friends and finally visited it to give a savoury treat to my tastebuds!\r\nKebabs and even chicken pieces in gravy had strong smoky flavour which made it a complete delight! Spices were perfect with tender succulent pieces cooked to perfection. Gravy was rich and creamy!\r\nN Biryani was sooo yummm! Just the thought of it is making me hungry !:P!\r\nIts a simple, clean place! Walls hv been painted very dark with dark curtains giving it a dull, dark feel! But then it doesnt make any difference as food is mashallah!","timer":"15-06-16 15:01:53","uid":"55","status":"1","user":null,"likes":[{"id":"106","uid":"151","reviewid":"122","time1":"15-06-16 16:07:38","status":"1"},{"id":"109","uid":"123","reviewid":"122","time1":"15-06-16 16:11:14","status":"1"},{"id":"127","uid":"143","reviewid":"122","time1":"2015-06-23 22:35:17","status":"1"}]},{"rid":"123","client_id":"1868","review":" The best food you can get in the entire city. Be it non veg, veg, starters you name it. Awesome experience. Its little bit expensive but totally worth it. Gajar ka halwa is delightful. In the end they give these mint shots, which has a paan like flavour. Its really good......","timer":"15-06-16 16:52:05","uid":"123","status":"1","user":{"uid":"123","name":"Satya Srk","email":"satyasrkk@gmail.com","uimg":"http:\/\/graph.facebook.com\/814774468604647\/picture","verify_flag":"1","social_flag":"fb","cityid":"1"},"likes":[{"id":"110","uid":"151","reviewid":"123","time1":"15-06-18 12:08:12","status":"0"},{"id":"124","uid":"149","reviewid":"123","time1":"15-06-18 17:12:23","status":"0"},{"id":"129","uid":"177","reviewid":"123","time1":"15-07-01 11:58:03","status":"1"}]}]}]}
     $scope.clientLoaded = true;*/

    /**
     * Set parameters for rating, and other objects for scope data.
     * @type {{}}
     */
    $scope.rating = {};
    $scope.rating.max = 5;
    $scope.formData = {};
    $scope.sms = {};
    $scope.bannerImage = {};
    $scope.notApprovedReviewStatus = Common.messages.REVIEWNOTAPPROVED;
    var toggleFlag = false;



    /**
     * Get client data and set rating, maps.
     */
    Common.showLoader();
    clientFactory.getClient($stateParams.clientId).success(function (data) {
        console.log('Ionic: Got Client Data');
        $scope.clientData = data.clientDetails[0];
        console.log(JSON.stringify(data.clientDetails[0]));
        $scope.rating.rate = $scope.clientData.rating || 0;
        if($scope.clientData.banners.length != 0) {
            $scope.bannerImage = $scope.bannerPrefix + $scope.clientData.banners[0].image;
        } else {
            $scope.bannerImage = 'img/client/banner-new.jpg';
        }

         /**
         * For Google Maps
         */
        $scope.map = {
            center: {
                latitude: $scope.clientData.latitude,
                longitude: $scope.clientData.longitude
            },
            zoom: 14,
            marker: {
                idKey: 1,
                center: {
                    latitude: $scope.clientData.latitude,
                    longitude: $scope.clientData.longitude
                }
            },
            options: {
                draggable:false,
                disableDoubleClickZoom: true,
                panControl: false,
                scaleControl: false,
                zoomControl: false
            }
        };
        Common.hideLoader();
        $scope.clientLoaded = true;
    }).error(function (data) {
        console.log('Error getting data');
    });


    $scope.openNavigate = function() {

        if (window.cordova) {
            launchnavigator.navigate([$scope.clientData.latitude, $scope.clientData.longitude], null, function () {
            }, function (err) {
            });
        }
    };

    $scope.toggleEdit = function () {
        $scope.editReview = !$scope.editReview;
    };

    $scope.putReview = function () {
        if (localStorage.getItem('guest_mode') == 'true') {
            Common.showToast(Common.messages.LOGINTOUSE);
            return;
        }
        if ($scope.formData.txtReview == null || $scope.formData.txtReview == '') {
            Common.showToast(Common.messages.REVIEWLENGTHERROR);
        }
        else if (paragraph.isGentle($scope.formData.txtReview).length > 0) {
            Common.showToast(Common.messages.SWEARWORDERROR);
        }
        else {
            console.log('Posting' + $scope.formData.txtReview + clientId);
            Common.showLoader();
            clientFactory.putReview($scope.formData.txtReview, clientId).success(function (data) {
                console.log($scope.clientData.review);
                $scope.clientData.review.push(data[0]);
                console.log($scope.clientData.review);
                if ($scope.editReview) {
                    $scope.editReview = !$scope.editReview;
                    Common.showToast(Common.messages.REVIEWUPDATED);
                }
                else {
                    Common.showToast(Common.messages.REVIEWADDED);
                }
                Common.hideLoader();
            });
            Common.hideLoader();
        }
    };

    $scope.deleteReview = function () {
        Common.showLoader();
        clientFactory.deleteReview($scope.myReview.rid).success(function () {
            console.log('Deleted ' + $scope.myReview.rid + ' successfully');
            $scope.hasReview = false;
            $scope.myReview = undefined;
            $scope.formData.txtReview = '';
            Common.hideLoader();
            Common.showToast(Common.messages.REVIEWDELETED);
        }).error(function () {

        })
    };

    $scope.$watch('myReview', function (newVal) {
        if ($scope.clientData != undefined && $scope.myReview != undefined) {
            $scope.formData.txtReview = newVal.review;
        }
    }, true);

    /**
     * Watches clientData. If no reviews present, sets a friendly message. Also, sets hasReview flag if the current
     * user has a review for this particular client. The hasReview flag is used for UI ng-shows.
     */
    $scope.$watch('clientData', function (newVal) {
        console.log('Checking');
        if ($scope.clientData != undefined) {
            if ($scope.clientData.review.length === 0 && typeof $scope.myReview === 'undefined') {
                $scope.reviewStatus = Common.messages.NOREVIEWS;
            } else {
                $scope.reviewStatus = '';
            }

            angular.forEach($scope.clientData.review, function (value, key, obj) {
                if (value.uid == userId) {
                    $scope.hasReview = true;
                    $scope.myReview = value;
                    obj.splice(key, 1);
                    console.log('Spliced' + JSON.stringify(value));
                }

                /* If the like/unlike is not clicked from the UI, run this loop (initialize likes) */
                if (!toggleFlag) {
                    value.likeCount = 0;
                    angular.forEach(value.likes, function (like) {
                        /* If the like is active (status = 1) */
                        if (parseInt(like.status) === 1) {
                            value.likeCount = value.likeCount + 1;
                            /* Check whether the current user has likes */
                            if (parseInt(like.uid) === parseInt(userId)) {
                                value.hasLike = true;
                            }
                        }
                    });
                    /* Set others as not liked */
                    if (typeof(value.hasLike) === 'undefined') {
                        value.hasLike = false;
                    }
                    /* Set status message (can be a better way) */
                    value.likeStatus = value.hasLike ? 'Unlike' : 'Like';
                }
            });
        }
    }, true);


    /**
     * Toggle like for a review.
     * @param review
     */
    $scope.toggleLike = function (review) {
        if (localStorage.getItem('guest_mode')  == 'true') {
            Common.showToast(Common.messages.LOGINTOUSE);
            return;
        }
        toggleFlag = true;
        if (review.hasLike) {
            review.likeCount = review.likeCount - 1;
            review.likeStatus = 'Like';
            review.hasLike = !review.hasLike;
        } else {
            review.likeCount = review.likeCount + 1;
            review.likeStatus = 'Unlike';
            review.hasLike = !review.hasLike;
        }
        clientFactory.toggleLike({
            'review_id': review.rid,
            'status': (+review.hasLike)
        }).success(function () {
            console.log('In toggle:');
            console.log(review);
        });
    };

    /**
     * Change the rating for a client.
     */

    $scope.changeRating = function () {
        if (localStorage.getItem('guest_mode') == 'false') {
            console.log($scope.rating.rate);
            clientFactory.postRating({
                'client_id': clientId,
                'rating': $scope.rating.rate
            }).success(function () {
                console.log('Rated!');
                Common.showToast(Common.messages.CLIENTRATED);
            });
        } else {
            Common.showToast(Common.messages.LOGINTOUSE);
        }
    };

    $scope.markFav = function () {
        if (localStorage.getItem('guest_mode') == 'true') {
            Common.showToast(Common.messages.LOGINTOUSE);
            return;
        }
        $scope.clientData.isFavourite = !$scope.clientData.isFavourite;
        clientFactory.postFav({
            'client_id': clientId,
            'fav': $scope.clientData.isFavourite
        }).success(function () {
            console.log('Fav Done.');
        });
    };

    /**
     * SMS Modal and related functions
     */

    $scope.sendMessage = function () {
        if ($scope.sms.mobileNumber.toString().length != 10 || !isANumber($scope.sms.mobileNumber)) {
            Common.showToast(Common.messages.INVALIDNUMBER);
        }
        else
        {
            clientFactory.sendMessage(clientId, $scope.sms.mobileNumber).success(function () {

                $scope.closeContactModal();
                Common.showToast(Common.messages.MESSAGESENT);
            }).error(function () {
                $scope.closeContactModal();
            })
        }
    };

    var isANumber = function(n) {
        var numStr = /^-?\d*?\d+$/;
        return numStr.test( n.toString() );
    };

    $ionicModal.fromTemplateUrl('contact-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.contactModal = modal
    });

    $scope.openContactModal = function () {
        $scope.contactModal.show()
    };

    $scope.closeContactModal = function () {
        $scope.sms.mobileNumber = "";
        $scope.contactModal.hide();
    };

    $scope.$on('$destroy', function () {
        $scope.contactModal.remove();
    });

    /**
     * End SMS Modal
     */

    $ionicModal.fromTemplateUrl('timing-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.timingModal = modal
    });

    $scope.openTimingModal = function () {
        $scope.timingModal.show()
    };

    $scope.closeTimingModal = function () {
        $scope.timingModal.hide();
    };

    $scope.addContact = function () {
        var phoneNumbers = [];
        var addresses = [];
        addresses[0] = new ContactAddress(false, 'work', $scope.clientData.address);
        phoneNumbers[0] = new ContactField('work', $scope.clientData.contact, true);
        $cordovaContacts.save({
            'displayName': $scope.clientData.Restaurant_Name,
            'phoneNumbers': phoneNumbers,
            'addresses': addresses
        }).then(function (result) {
            Common.showToast(Common.messages.CONTACTADDED);
        }, function (err) {
            Common.showToast(err);
        });
    };
    $scope.callClient = function (phoneNumber) {
        if (window.cordova != undefined) {
            window.plugins.CallNumber.callNumber(function() {}, function() {}, phoneNumber);
        }
    };

    $scope.callListData = [];
    $scope.showCallList = function () {
        $scope.callListData = $scope.clientData.contact.split(',');
        if ($scope.callListData.length == 1){
            $scope.callClient($scope.clientData.contact);
        } else {
            var listPopup = $ionicPopup.show({
                templateUrl: 'templates/call-modal.html',
                title: 'Contact List',
                scope: $scope,
                buttons: [
                    {text: 'Cancel'}
                ]
            });
        }
    };

    $ionicModal.fromTemplateUrl('image-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.photosModal = modal;
    });

    $ionicModal.fromTemplateUrl('menu-modal.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function(modal) {
        $scope.menusModal = modal;
    });



    $scope.openMenusModal = function() {
        $ionicSlideBoxDelegate.$getByHandle('menus').slide(0);
        $scope.menusModal.show();
    };

    $scope.closeMenusModal = function() {
        $scope.menusModal.hide();
    };

    $scope.$on('$ionicView.enter', function(){
        $scope.nextMenu = function() {
            $ionicSlideBoxDelegate.$getByHandle('menus').next();
        };

        $scope.previousMenu = function() {
            $ionicSlideBoxDelegate.$getByHandle('menus').previous();
        };

        $scope.goToMenuSlide = function(index) {
            $scope.menusModal.show();
            $timeout(function() {
                $ionicSlideBoxDelegate.$getByHandle('menus').slide(index);
            }, 100  )

        };


        $scope.openPhotosModal = function() {
            $ionicSlideBoxDelegate.$getByHandle('photos').slide(0);
            $scope.photosModal.show();
        };

        $scope.closePhotosModal = function() {
            $scope.photosModal.hide();
        };


        // Call this functions if you need to manually control the slides
        $scope.nextPhoto = function() {
            $ionicSlideBoxDelegate.$getByHandle('photos').next();
        };

        $scope.previousPhoto = function() {
            $ionicSlideBoxDelegate.$getByHandle('photos').previous();
        };

        $scope.goToPhotoSlide = function(index) {
            $scope.photosModal.show();
            console.log($ionicSlideBoxDelegate.$getByHandle('photos'));
            $timeout(function() {
                $ionicSlideBoxDelegate.$getByHandle('photos').slide(index);
            },   100);
        };
    });

    // Call this functions if you need to manually control the slides

    // Cleanup the modal when we're done with it!
    $scope.$on('$destroy', function() {
        $scope.photosModal.remove();
        $scope.menusModal.remove();
        $scope.timingModal.remove();
        $scope.contactModal.remove();
    });

});
